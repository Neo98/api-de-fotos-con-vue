import Vue from 'vue'
import Router from 'vue-router';
import Inicio from '../components/inicio.vue';
import Formulario from '../components/formulario.vue';
Vue.use(Router);


const routes = [
    {
        path:'/',
        name:'busqueda',
        component:Inicio
    },
    {
        path:'/formulario/:id',
        name:'formulario',
        component:Formulario
    }
];

const router =  new Router({
    mode:'history',
    base:process.env.BASE_URL,
    routes:routes
});

export default router;