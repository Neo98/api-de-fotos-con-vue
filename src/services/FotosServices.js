import axios from "axios";

export default class FotosServices{
    url = "https://picsum.photos/";
    getFotografias(){
        return axios.get(this.url + 'v2/list');
    }
    getFoto(id){
        return axios.get(this.url + 'id/'+id+'/info');
    }
    getDataFoto(id,ancho,alto){
        // https://picsum.photos/id/0/5616/3744
        return axios.get(this.url + 'id/' + id + '/' + ancho + '/' + alto);
    }
}