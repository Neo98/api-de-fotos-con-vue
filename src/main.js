import Vue from 'vue'
import App from './App.vue'
import router from './rutas/rutas.js'
// import Router from 'vue-router'

Vue.config.productionTip = false
// Vue.use(Router);
// const routes = [
//   {
//       path:'/',
//       name:'inicio',
//       component: 
//   }
// ];


new Vue({
  router,
  render: h => h(App),
}).$mount('#app')


// const router =  new Router({
//   mode:'history',
//   base:process.env.BASE_URL,
//   routes:routes
// });

// export default router;